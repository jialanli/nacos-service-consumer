package com.aliware.edas;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author wanglan
 */

// 使用 @FeignClient 构造 FeignClient,name参数意味着该接口将调用到哪个服务(请求会被哪个服务接收)
@FeignClient(name = "service-provider")
public interface EchoService {
	// 采用Feign方式需要写好该接口
    @RequestMapping(value = "/echo/{str}", method = RequestMethod.GET)
    String echo(@PathVariable("str") String str);
}