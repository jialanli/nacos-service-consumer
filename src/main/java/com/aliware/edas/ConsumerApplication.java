package com.aliware.edas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @author wanglan
 * Feign是一个java实现的 HTTP 客户端，用于简化 RESTful调用
   @EnableFeignClients和 @FeignClient 完成负载均衡请求
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients			// 使用 @EnableFeignClients开启 Feign功能
public class ConsumerApplication {

	// RestTemplate是Spring提供的用于访问 REST 服务的客户端，提供了多种便捷访问远程 HTTP服务的方法
	// 使用 @LoadBalanced注解修改构造的 RestTemplate，使其拥有一个负载均衡功能
	@LoadBalanced	
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	public static void main(String[] args) {
		SpringApplication.run(ConsumerApplication.class, args);
	}
}
